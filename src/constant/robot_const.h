#ifndef ROBOT_CONST_H
#define ROBOT_CONST_H

enum JointIndex {
  LIFT_JOINT,
  YAW_JOINT,
  ROLL_JOINT,
  PITCH1_JOINT,
  PITCH2_JOINT,
  NUM_JOINT
};

// digital input
enum DigitalInput {
  POWER_SW,
  NUM_DIGITAL_IN
};

// digital output
enum DigitalOutput {
  POWER_12V_24V,
  SLIDER_UP,
  SLIDER_DOWN,
  NUM_DIGITAL_OUT
};

#define CONTROLLER_CYCLE_TIME_MS 10

#endif // ROBOT_CONST_H