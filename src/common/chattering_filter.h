#ifndef CHATTERING_FILTER_H
#define CHATTERING_FILTER_H

class ChatteringFilter {
public:
  ChatteringFilter();
  ChatteringFilter(float sampling_time, float chattering_time, bool init_status = false);
  bool Update(bool status);
  bool GetStatus();
  void Reset();
private:
  bool pre_status_;
  unsigned int chattering_counter_;
  unsigned int chattering_limit_;
};

#endif // CHATTERING_FILTER_H