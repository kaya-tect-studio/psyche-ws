#include "chattering_filter.h"

ChatteringFilter::ChatteringFilter() {}

ChatteringFilter::ChatteringFilter(float sampling_time, float chattering_time, bool init_status) :
  pre_status_(init_status),
  chattering_counter_(0),
  chattering_limit_(static_cast<unsigned int>(chattering_time / sampling_time)) {}

bool ChatteringFilter::Update(bool status) {
  if (status != pre_status_) {
    chattering_counter_++;
    if (chattering_counter_ == chattering_limit_) {
      pre_status_ = status;
      chattering_counter_ = 0;
      return true;
    }
  } else {
    chattering_counter_ = 0;
  }
  return false;
}

bool ChatteringFilter::GetStatus() {
  return pre_status_;
}

void ChatteringFilter::Reset() {
  pre_status_ = false;
  chattering_counter_ = 0;
}