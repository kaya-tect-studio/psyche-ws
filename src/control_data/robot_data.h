#ifndef ROBOT_DATAA_H
#define ROBOT_DATAA_H

#include "constant/robot_const.h"
#include <stdint.h>

struct JointData {
  std::array<float, NUM_JOINT> q;
};

struct StateData {
  JointData joint;
  std::array<int8_t, NUM_DIGITAL_IN> digital_in;
};

struct RefData {
  JointData joint;
  std::array<int8_t, NUM_DIGITAL_OUT> digital_out;
};

#endif // ROBOT_DATAA_H