#ifndef IO_DATA_H
#define IO_DATA_H

#include <array>
#include <stdint.h>
#include "constant/robot_const.h"

struct InputData {
  std::array<int8_t, NUM_DIGITAL_IN> digital_in;
  std::array<int32_t, NUM_JOINT> encoder_binary;
};

struct OutputData {
  std::array<int8_t, NUM_DIGITAL_OUT> digital_out;
  std::array<int8_t, NUM_JOINT> motor_enable; 
  std::array<int32_t, NUM_JOINT> motor_binary; 
};

#endif // IO_DATA_H