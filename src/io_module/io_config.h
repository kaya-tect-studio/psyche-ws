#ifndef IO_CONFIG_H
#define IO_CONFIG_H

#include "constant/robot_const.h"
#include <array>
#include <Arduino.h>

// #define SIMULATION_MODE

/* num define */
#define NUM_ENCODER_IN 4
#define NUM_PWM_OUT 4

/**
 * @brief pin define
 * @note see https://os.mbed.com/platforms/ST-Nucleo-F767ZI/
 */
// Digital In
#define DIGITAL_IN0_PIN 2
// Digital Out
#define DIGITAL_OUT0_PIN 3
#define DIGITAL_OUT1_PIN 4
#define DIGITAL_OUT2_PIN 5
// Encoder In
#define ENCODER_IN0_PIN A0
#define ENCODER_IN1_PIN A1
#define ENCODER_IN2_PIN A2
#define ENCODER_IN3_PIN A3
// PWM Out
#define PWM_OUT0_PIN 11
#define PWM_OUT1_PIN 10
#define PWM_OUT2_PIN 9
#define PWM_OUT3_PIN 8

enum IoType {
  AI,
  DI,
  AO,
  DO,
  MOTOR,
  ENCODER,
};

enum IoIndex {
  IO_0,
  IO_1,
  IO_2,
  IO_3,
  IO_4,
  IO_NONE
};

struct IoTable {
  uint8_t device_index;
  uint8_t device_ch;
  IoType type;
  uint8_t index;
};

static std::array<IoTable, NUM_DIGITAL_IN> DIGITAL_IN_TABLE = {{
  {0, DIGITAL_IN0_PIN,  IoType::DI, DigitalInput::POWER_SW},
}};

static std::array<IoTable, NUM_DIGITAL_OUT> DIGITAL_OUT_TABLE = {{
  {0, DIGITAL_OUT0_PIN,  IoType::DO, DigitalOutput::POWER_12V_24V},
  {0, DIGITAL_OUT1_PIN,  IoType::DO, DigitalOutput::SLIDER_UP},
  {0, DIGITAL_OUT2_PIN,  IoType::DO, DigitalOutput::SLIDER_DOWN}
}};

static std::array<IoTable, NUM_ENCODER_IN> ENCODER_IN_TABLE = {{
  {0, ENCODER_IN0_PIN,  IoType::ENCODER, JointIndex::YAW_JOINT},
  {0, ENCODER_IN1_PIN,  IoType::ENCODER, JointIndex::ROLL_JOINT},
  {0, ENCODER_IN2_PIN,  IoType::ENCODER, JointIndex::PITCH1_JOINT},
  {0, ENCODER_IN3_PIN,  IoType::ENCODER, JointIndex::PITCH2_JOINT}
}};

static std::array<IoTable, NUM_PWM_OUT> PWM_OUT_TABLE = {{
  {0, PWM_OUT0_PIN,  IoType::MOTOR, JointIndex::YAW_JOINT},
  {1, PWM_OUT1_PIN,  IoType::MOTOR, JointIndex::ROLL_JOINT},
  {2, PWM_OUT2_PIN,  IoType::MOTOR, JointIndex::PITCH1_JOINT},
  {3, PWM_OUT3_PIN,  IoType::MOTOR, JointIndex::PITCH2_JOINT}
}};

#endif // IO_CONFIG_H