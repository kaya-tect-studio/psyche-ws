#include "io_config.h"
#include "io_module.h"

/**
 * @class IoModule
 */
IoModule::IoModule() :
  counter_(0){}

void IoModule::Init() {
  // Init DigitalIn
  for (size_t i = 0; i < DIGITAL_IN_TABLE.size(); i++) {
    const IoTable& table = DIGITAL_IN_TABLE[i];
    pinMode(table.device_ch, INPUT_PULLUP);
  }
  // Init DigitalOut
  for (size_t i = 0; i < DIGITAL_OUT_TABLE.size(); i++) {
    const IoTable& table = DIGITAL_OUT_TABLE[i];
    pinMode(table.device_ch, OUTPUT);
    digitalWrite(table.device_ch, LOW);
  }
  // Init PWM
  for (size_t i = 0; i < PWM_OUT_TABLE.size(); i++) {
    const IoTable& table = PWM_OUT_TABLE[i];
    servo_[table.device_index].attach(table.device_ch);
  } 
}

void IoModule::UpdateInput(InputData& input) {
  /* Digital Input */
  for (size_t i = 0; i < DIGITAL_IN_TABLE.size(); i++) {
    const IoTable& table = DIGITAL_IN_TABLE[i];
    input.digital_in[table.index] = digitalRead(table.device_ch);
  }

  /* Encoder */
  for (size_t i = 0; i < ENCODER_IN_TABLE.size(); i++) {
    const IoTable& table = ENCODER_IN_TABLE[i];
    input.encoder_binary[table.index] = analogRead(table.device_ch);
  }
}

void IoModule::UpdateOutput(const OutputData& output) {
  /* Digital Output */
  for (size_t i = 0; i < DIGITAL_OUT_TABLE.size(); i++) {
    const IoTable& table = DIGITAL_OUT_TABLE[i];
    digitalWrite(table.device_ch, output.digital_out[table.index]);
  }
  /* PWM */
  for (size_t i = 0; i < PWM_OUT_TABLE.size(); i++) {
    const IoTable& table = PWM_OUT_TABLE[i];
    if (servo_[table.device_index].attached() && !output.motor_enable[table.index]) {
      servo_[table.device_index].detach();
    } else if (!servo_[table.device_index].attached() && output.motor_enable[table.index]) {
      servo_[table.device_index].attach(table.device_ch);
    }
    servo_[table.device_index].writeMicroseconds((int)output.motor_binary[table.index]);
  }
}