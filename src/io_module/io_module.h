#ifndef IO_MODULE_H
#define IO_MODULE_H

#include "control_data/io_data.h"
#include "io_config.h"
#include <Servo.h>

class IoModule {
public:
  IoModule();
  void Init();
  void UpdateInput(InputData& input);
  void UpdateOutput(const OutputData& output);

private:
  uint64_t counter_;
  std::array<Servo, NUM_PWM_OUT> servo_;
};

#endif // IO_MODULE_H