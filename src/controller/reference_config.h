#ifndef REFERENCE_CONFIG_H
#define REFERENCE_CONFIG_H

#include <array>
#include "math/math_const.h"
#include "constant/robot_const.h"
#include "math/wave.h"

struct ReferenceConfig {
  float operation_time;
  std::array<WaveForm, NUM_JOINT> waves;
};

void GetBigWave(ReferenceConfig& config, int num_wave);

void GetSmallWave(ReferenceConfig& config, int num_wave);

void GetBelowScratchWave(ReferenceConfig& config, int num_wave);

void GetStepDownWave(ReferenceConfig& config, int num_wave);

void GetTriangleWave(ReferenceConfig& config, int num_wave);

void GetRubWave(ReferenceConfig& config, int num_wave);

void GetStopWave(ReferenceConfig& config, int num_wave);

void GetReferenceConfig(int index, int num_wave, ReferenceConfig& config);

#endif // REFERENCE_CONFIG_H