#ifndef ARM_INTERFACE_H
#define ARM_INTERFACE_H

#include "control_data/io_data.h"
#include "control_data/robot_data.h"
#include "hardware_config.h"
#include "motion_generator.h"

class ArmInterface {
public:
  ArmInterface();
  void Init();
  void Update(const InputData& input, OutputData& output);
  void Config();
  void PowerOn();
  void PowerOff();
  void ControlOn();
  void ControlOff();
  RefData& GetRefData();
  StateData& GetStateData();
private:
  void InputToState(const InputData& input, StateData& state);
  void RefToOutput(const RefData& ref, OutputData& output);
  HardwareConfig hardware_config_;
  // ControlConfig control_config_;
  MainGenerator motion_;
  RefData pre_ref_;
  StateData pre_state_;
  long long control_counter_;
  bool power_status_;
};

#endif // ARM_INTERFACE_H