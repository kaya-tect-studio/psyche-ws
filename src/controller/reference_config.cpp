#include "reference_config.h"

//#define RIGHT
#ifdef RIGHT
constexpr float YAW_OFFSET = 0.0f * DEG_TO_RAD;
constexpr float YAW_STEP = - 15.0f * DEG_TO_RAD; // Right = -15 deg, Left = 15 deg
constexpr float ROLL_AMP = - 5.0 * DEG_TO_RAD;  // Right = -5 deg, Left = 5 deg
#else // LEFT
constexpr float YAW_OFFSET = 5.0f * DEG_TO_RAD;
constexpr float YAW_STEP = 15.0f * DEG_TO_RAD; // Right = -15 deg, Left = 15 deg
constexpr float ROLL_AMP = 5.0 * DEG_TO_RAD;  // Right = -5 deg, Left = 5 deg
#endif

// time = (n + 0.25) / freq
void GetBigWave(ReferenceConfig& config, int num_wave) {
  config.operation_time = (num_wave + 0.25) * 14.28f; // n = 3
#if defined(ARDUINO_AVR_UNO)
  config.waves = {
#else
  config.waves = {{
#endif
    {WaveType::SIN, 1.0f, 0.3f, 0.1f, 0.0f}, // 速度目標値
    {WaveType::CONST, 0.0f, YAW_STEP + YAW_OFFSET, 0.0f, 0.0f},
    {WaveType::JERKMIN, ROLL_AMP, 0.0f, 0.07f, - 90.0f * DEG_TO_RAD},
    {WaveType::SIN, 30.0f * DEG_TO_RAD, 0.0f, 0.07f, 0.0f},
    {WaveType::SIN, 50.0f * DEG_TO_RAD, 0.0f, 0.07f, -45.0f * DEG_TO_RAD}
#if defined(ARDUINO_AVR_UNO)
  };
#else
  }};
#endif
}

void GetSmallWave(ReferenceConfig& config, int num_wave) {
  config.operation_time = (num_wave + 0.25) * 14.28f; // n = 2
#if defined(ARDUINO_AVR_UNO)
  config.waves = {
#else
  config.waves = {{
#endif
    {WaveType::SIN, 1.0f, 0.3f, 0.1f, 0.0f}, // 速度目標値
    {WaveType::CONST, 0.0f,  YAW_STEP + YAW_OFFSET, 0.0f, 0.0f},
    {WaveType::JERKMIN, ROLL_AMP, 0.0f, 0.07f, - 90.0f * DEG_TO_RAD},
    {WaveType::SIN, 15.0f * DEG_TO_RAD, 0.0f, 0.07f, 0.0f},
    {WaveType::SIN, 25.0f * DEG_TO_RAD, 0.0f, 0.07f, 0.0f}
#if defined(ARDUINO_AVR_UNO)
  };
#else
  }};
#endif
}

void GetBelowScratchWave(ReferenceConfig& config, int num_wave) {
  config.operation_time = (num_wave + 0.25) * 1.0f; // n = 20
#if defined(ARDUINO_AVR_UNO)
  config.waves = {
#else
  config.waves = {{
#endif
    {WaveType::CONST, 0.0f, - 1.0f, 0.0f, 0.0f}, // 速度目標値
    {WaveType::CONST, 0.0f, YAW_STEP + YAW_OFFSET, 0.0f, 0.0f},
    {WaveType::CONST, 0.0f , 0.0f, 0.03f, 0.0f},
    {WaveType::CONST, 0.0f, 30.0f * DEG_TO_RAD, 0.0f, 0.0f},
    {WaveType::ECC_SIN, 3.0f * DEG_TO_RAD, 40.0f * DEG_TO_RAD, 1.0f, 0.0f}
#if defined(ARDUINO_AVR_UNO)
  };
#else
  }};
#endif
}

void GetStepDownWave(ReferenceConfig& config, int num_wave) {
  config.operation_time = (num_wave + 0.25) * 20.0f; // n = 2
#if defined(ARDUINO_AVR_UNO)
  config.waves = {
#else
  config.waves = {{
#endif
    {WaveType::SIN, 1.0f, 0.3f, 0.1f, 0.0f}, // 速度目標値
    {WaveType::CONST, 0.0f, YAW_STEP + YAW_OFFSET, 0.0f, 0.0f},
    {WaveType::CONST, 0.0f, 0.0f, 0.0f, 0.0f},
    {WaveType::DOWN_STEP, 20.0f * DEG_TO_RAD, 0.0f, 0.05f, 0.0f},
    {WaveType::DOWN_STEP, 30.0f * DEG_TO_RAD, 0.0f, 0.05f, 0.0f}
#if defined(ARDUINO_AVR_UNO)
  };
#else
  }};
#endif
}

void GetTriangleWave(ReferenceConfig& config, int num_wave) {
  config.operation_time = (num_wave + 0.25) * 20.0f; // n = 2
#if defined(ARDUINO_AVR_UNO)
  config.waves = {
#else
  config.waves = {{
#endif
    {WaveType::SIN, 1.0f, 0.3f, 0.1f, 0.0f}, // 速度目標値
    {WaveType::CONST, 0.0f, YAW_STEP + YAW_OFFSET, 0.1f, 0.0f},
    {WaveType::JERKMIN, ROLL_AMP, - ROLL_AMP, 0.05f, - 90.0f * DEG_TO_RAD},
    {WaveType::SIN, 30.0f * DEG_TO_RAD, 0.0f, 0.05f, 0.0f},
    {WaveType::SIN, 50.0f * DEG_TO_RAD, 0.0f, 0.05, 0.0f}
#if defined(ARDUINO_AVR_UNO)
  };
#else
  }};
#endif
}

void GetRubWave(ReferenceConfig& config, int num_wave) {
  config.operation_time = (num_wave + 0.25) * 16.6f; // n = 3
#if defined(ARDUINO_AVR_UNO)
  config.waves = {
#else
  config.waves = {{
#endif
    {WaveType::SIN, 1.0f, 0.3f, 0.1f, 0.0f}, // 速度目標値
    {WaveType::SMOOTH_STEP, YAW_STEP, YAW_OFFSET, 0.06f, 0.0f},
    {WaveType::CONST, 0.0f, 0.0f * DEG_TO_RAD, 0.0f, 0.0f},
    {WaveType::SIN, 25.0f * DEG_TO_RAD, 0.0f, 0.06f, 90.0f * DEG_TO_RAD},
    {WaveType::SIN, 17.5f * DEG_TO_RAD, 37.5f * DEG_TO_RAD, 0.06f, 90.0f * DEG_TO_RAD}
#if defined(ARDUINO_AVR_UNO)
  };
#else
  }};
#endif
}

void GetStopWave(ReferenceConfig& config, int num_wave) {
  config.operation_time = 180.0f;
#if defined(ARDUINO_AVR_UNO)
  config.waves = {
#else
  config.waves = {{
#endif
    {WaveType::CONST, 0.0f, - 1.0f, 0.0f, 0.0f}, // 速度目標値
    {WaveType::CONST, 0.0f, YAW_OFFSET, 0.1f, 0.0f},
    {WaveType::CONST, 0.0f, 0.0f, 0.1f, 0.0f},
    {WaveType::CONST, 0.0f, 30.0f * DEG_TO_RAD, 0.1f, 0.0f},
    {WaveType::CONST, 0.0f, 50.0f * DEG_TO_RAD, 0.1f, 0.0f}
#if defined(ARDUINO_AVR_UNO)
  };
#else
  }};
#endif
}

void GetReferenceConfig(int index, int num_wave, ReferenceConfig& config) {
  if (index == 0) {
    GetBigWave(config, num_wave);
  } else if (index == 1) {
    GetSmallWave(config, num_wave);
  } else if (index == 2) {
    GetBelowScratchWave(config, 20);
  } else if (index == 3) {
    GetStepDownWave(config, num_wave);
  } else if (index == 4) {
    GetTriangleWave(config, num_wave);
  } else if (index == 5) {
    GetRubWave(config, num_wave);
  } else {
    GetStopWave(config, num_wave);
  }
}