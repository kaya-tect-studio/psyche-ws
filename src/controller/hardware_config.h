#ifndef HARDWARE_CONFIG_H
#define HARDWARE_CONFIG_H

#include <array>
#include <stdint.h>
#include "constant/robot_const.h"
#include "math/math_const.h"

//#define MG996R
//#define ASMC_04

/**
 * @brief encoder config convert voltage to pos
 */
struct EncoderConfig {
  std::array<unsigned int, NUM_JOINT> resolution; // ppr (pulse per rotate)
  std::array<float, NUM_JOINT> offset;            // rad (encoder angle with joint zero)
  std::array<float, NUM_JOINT> encoder_to_joint;  // -
};

/**
 * @brief actuator config convert pos to binary
 */
struct ActuatorConfig {
  std::array<uint16_t, NUM_JOINT> offset;      // bit (actuator binary with joint zero)
  std::array<float, NUM_JOINT> position_to_binary;  // /rad
};

struct JointConfig {
  std::array<float, NUM_JOINT> q_min;
  std::array<float, NUM_JOINT> q_max;
};

struct HardwareConfig {
  EncoderConfig encoder;
  ActuatorConfig actuator;
  //JointConfig joint;
};

inline void GetHardwareConfig(HardwareConfig& config) {
  /* encoder */
#ifdef MG996R
  // 1350 = 1023 * 3.3 / 5 * 2
  config.encoder.resolution = {{1, 1350, 1350, 1350, 1350}};
  config.encoder.encoder_to_joint = {{1.0f, 1.0f, 1.0f, 1.0f, 1.0f}}; // 180 / 180 deg
#else
  // 1882 = 1023 * 4.6 / 5 * 2
  config.encoder.resolution = {{1, 1882, 1882, 1882, 1882}};
  config.encoder.encoder_to_joint = {{1.0f, - 1.44f, - 1.44f, 1.44f, - 1.44f}}; // 130 / 90 deg
#endif
  config.encoder.offset = {{0.0f, PI_2, PI_2, PI_2, PI_2}};
  /* actuator */
  //config.actuator.gear_ratio = {{1.0f, 1.0f, 1.0f, 1.0f, 1.0f}};
  config.actuator.offset = {{0, 1500, 1500, 1500, 1500}};
#ifdef MG996R
  config.actuator.position_to_binary = {{1.0f, 1000 / PI_2, 1000 / PI_2, 1000 / PI_2, 1000 / PI_2}};
#else
  config.actuator.position_to_binary = {{1.0f, 1000 / (130.0f * DEG_TO_RAD), 1000 / (130.0f * DEG_TO_RAD), - 1000 / (130.0f * DEG_TO_RAD), 1000 / (130.0f * DEG_TO_RAD)}};
#endif
  //config.joint.q_min = {{-180.0f * DEG_TO_RAD, -25.0f * DEG_TO_RAD, -25.0f * DEG_TO_RAD, -77.0f * DEG_TO_RAD, -53.0f * DEG_TO_RAD}};
  //config.joint.q_max = {{180.0f * DEG_TO_RAD, 25.0f * DEG_TO_RAD, 25.0f * DEG_TO_RAD, 35.0f * DEG_TO_RAD, 75.0f * DEG_TO_RAD}};
}

#endif // HARDWARE_CONFIG_H