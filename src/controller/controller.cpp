#include "controller.h"
#include "control_data/robot_data.h"
#include "math/math_const.h"

Controller::Controller():
  counter_(0),
  state_machine_(CONTROLLER_CYCLE_TIME_MS * MS_TO_S) {}

void Controller::Init() {
  arm_.Init();
}

void Controller::Update(const InputData& input, OutputData& output) {
  /* update fsm */
  state_machine_.Update(input);
  if (state_machine_.HasChangedState()) {
    if (state_machine_.GetState() == StateMachineState::RUN_POWER_ON) {
      ReactPowerOn();
    } else if (state_machine_.GetState() == StateMachineState::CONTORL_ON) {
      ReactControlOn();
    } else if (state_machine_.GetState() == StateMachineState::CONTORL_OFF) {
      ReactControlOff();
    } else if (state_machine_.GetState() == StateMachineState::POWER_OFF) {
      ReactPowerOff();
    }
  }
  /* update arm */
  arm_.Update(input, output);

  /* update enable */
  for (size_t i = 0; i < output.motor_enable.size(); i++) {
    output.motor_enable[i] = (state_machine_.GetState() == StateMachineState::CONTORL_ON) ? true : false;
  }

  /* power off */
  if (state_machine_.GetState() == StateMachineState::RUN_POWER_OFF) {
    output.digital_out[DigitalOutput::SLIDER_DOWN] = true;
    output.digital_out[DigitalOutput::SLIDER_UP] = false;    
  }

  /* update variables */
  counter_++;
}

void Controller::ReactPowerOn() {
  arm_.PowerOn();
}

void Controller::ReactPowerOff() {
  arm_.PowerOff();
}

void Controller::ReactControlOn() {
  arm_.Config();
  arm_.ControlOn();
}

void Controller::ReactControlOff() {
  arm_.ControlOff();
}