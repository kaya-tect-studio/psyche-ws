#ifndef MOTION_GENERATOR_H
#define MOTION_GENERATOR_H

#include <array>
#include "control_data/robot_data.h"
#include "math/wave.h"
#include "math/smooth_path.h"
#include "state_machine.h"
#include "reference_config.h"

enum State {
  INITIALIZE,
  STOP,
  RUN
};

class GeneratorBase {
  public:
  GeneratorBase();
  virtual ~GeneratorBase();
  virtual void Config();
  virtual void Update(float time, const JointData& state, const JointData& pre_re, JointData& ref);
  void StartRequest();
  void StopRequest();
  void CompleteConfig();
  protected:
  State state_;
};

class MainGenerator : public GeneratorBase {
  public:
  MainGenerator(uint8_t num_of_joints);
  ~MainGenerator();
  void Config();
  void Update(float time, const JointData& state, const JointData& pre_ref, JointData& ref) override;
  private:
  ReferenceConfig reference_config_;
  uint8_t reference_index_;
  std::array<Wave,  NUM_JOINT> wave_;
  std::array<SmoothingPath<float>, NUM_JOINT> q_traj_;
  
  bool is_first_loop_;
  float start_time_;
};

#endif // MOTION_GENERATOR_H