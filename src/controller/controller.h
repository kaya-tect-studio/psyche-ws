#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "control_data/io_data.h"
#include "arm_interface.h"
#include "state_machine.h"

class Controller {
public:
  Controller();
  void Init();
  void Update(const InputData& input, OutputData& output);
private:
  void ReactPowerOn();
  void ReactPowerOff();
  void ReactControlOn();
  void ReactControlOff();
  long long counter_;
  ArmInterface arm_;
  StateMachine state_machine_;
};

#endif // CONTROLLER_H