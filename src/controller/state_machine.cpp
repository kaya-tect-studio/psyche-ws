#include "state_machine.h"
#include "constant/robot_const.h"

constexpr float POWER_CHANGE_TIME = 3.0;
constexpr float POWER_OFF_CHANGE_TIME = 60.0;
constexpr float CHATTERING_TIME = 0.1f;

StateMachine::StateMachine(float sample_time) :
  sample_time_(sample_time),
  state_counter_(0),
  now_state_(StateMachineState::BOOT),
  pre_state_(StateMachineState::BOOT),
  digital_in_filter_{ChatteringFilter(sample_time, CHATTERING_TIME, false)} {}

void StateMachine::Update(const InputData& input) {
  // filter
  InputData input_filtered = input;
  for (size_t i = 0; i < digital_in_filter_.size(); i++) {
    digital_in_filter_[i].Update(static_cast<bool>(input.digital_in[i]));
    input_filtered.digital_in[i] = digital_in_filter_[i].GetStatus();
  }
  // restore pre state
  pre_state_ = now_state_;
  // update state
  if (now_state_ == StateMachineState::BOOT) {
    // change to POWER_OFF
    if (input_filtered.digital_in[DigitalInput::POWER_SW]) {
      SetState(StateMachineState::POWER_OFF);
    }
  } else if (now_state_ == StateMachineState::POWER_OFF) {
    // change to RUN_POWER_ON
    if (!input_filtered.digital_in[DigitalInput::POWER_SW]) {
      SetState(StateMachineState::RUN_POWER_ON);
    }
  } else if (now_state_ == StateMachineState::RUN_POWER_ON) {
    // change to POWER_ON
    if (!input_filtered.digital_in[DigitalInput::POWER_SW] && (state_counter_ * sample_time_ > POWER_CHANGE_TIME)) {
      SetState(StateMachineState::POWER_ON);
    }
  } else if (now_state_ == StateMachineState::POWER_ON) { // CONTROL_OFF
    // change to CONTROL_ON
    if (!input_filtered.digital_in[DigitalInput::POWER_SW]) {
      SetState(StateMachineState::CONTORL_ON);
    } else {
      SetState(StateMachineState::RUN_POWER_OFF);
    }
  } else if (now_state_ == StateMachineState::CONTORL_ON) {
    // change to POWER_ON
    if (input_filtered.digital_in[DigitalInput::POWER_SW]) {
      SetState(StateMachineState::CONTORL_OFF);
    }
  } else if (now_state_ == StateMachineState::RUN_POWER_OFF) {
    if (state_counter_ * sample_time_ > POWER_OFF_CHANGE_TIME) {
      SetState(StateMachineState::POWER_OFF);
    }
  } else {

  }

  // reset counter
  if (HasChangedState()) {
    state_counter_ = 0;
  }

  // update 
  state_counter_++;
}

void StateMachine::SetState(StateMachineState state) {
  now_state_ = state;
}

const StateMachineState& StateMachine::GetState() const {
  return now_state_;
}

bool StateMachine::HasChangedState() {
  return (now_state_ != pre_state_);
}