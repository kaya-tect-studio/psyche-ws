#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "control_data/io_data.h"
#include "common/chattering_filter.h"

enum StateMachineState {
  BOOT,
  POWER_OFF,
  RUN_POWER_ON,
  POWER_ON, // CONTROL_OFF
  CONTORL_OFF = POWER_ON,
  CONTORL_ON,
  RUN_POWER_OFF
};

class StateMachine {
public:
  StateMachine(float sample_time);
  void Update(const InputData& input);
  const StateMachineState& GetState() const;
  void SetState(StateMachineState state);
  bool HasChangedState();

private:
  float sample_time_;
  long long state_counter_;
  StateMachineState now_state_;
  StateMachineState pre_state_;
  std::array<ChatteringFilter, NUM_DIGITAL_IN> digital_in_filter_;
};

#endif // STATE_MACHINE_H