#include "arm_interface.h"

ArmInterface::ArmInterface() :
  motion_(NUM_JOINT) {}

void ArmInterface::Init() {
  GetHardwareConfig(hardware_config_);
  control_counter_ = 0;
}

void ArmInterface::Update(const InputData& input, OutputData& output) {
  /* variables */
  StateData state;
  RefData ref;
  float ctrl_time = control_counter_ * CONTROLLER_CYCLE_TIME_MS * MS_TO_S;
  /* update input */
  InputToState(input, state);
  /* update joint ref */
  motion_.Update(ctrl_time, state.joint, pre_ref_.joint, ref.joint);
  /* uodate digital out ref */
  ref.digital_out[DigitalOutput::POWER_12V_24V] = power_status_;
  ref.digital_out[DigitalOutput::SLIDER_UP] = (ref.joint.q[JointIndex::LIFT_JOINT] > 0.0f);
  ref.digital_out[DigitalOutput::SLIDER_DOWN] = (ref.joint.q[JointIndex::LIFT_JOINT] < 0.0f);
  /* update output */
  RefToOutput(ref, output);
  /* update variables */
  pre_state_ = state;
  pre_ref_ = ref;
  control_counter_++;
}

void ArmInterface::Config() {
  // control_config_ = config;
  motion_.Config();
  motion_.CompleteConfig();
}

void ArmInterface::PowerOn() {
  power_status_ = true;
}

void ArmInterface::PowerOff() {
  power_status_ = false;
}

void ArmInterface::ControlOn() {
  control_counter_ = 0;
  motion_.StartRequest();
}

void ArmInterface::ControlOff() {
  motion_.StopRequest();
}

RefData& ArmInterface::GetRefData() {
  return pre_ref_;
}

StateData& ArmInterface::GetStateData() {
  return pre_state_;
}

void ArmInterface::InputToState(const InputData& input, StateData& state) {
  // joint
  for (size_t i = 0; i < state.joint.q.size(); i++) {
    float encoder_pos = 2.0f * PI * input.encoder_binary[i] / hardware_config_.encoder.resolution[i] - hardware_config_.encoder.offset[i];
    state.joint.q[i] = hardware_config_.encoder.encoder_to_joint[i] * encoder_pos;
  }
  // digital input
  state.digital_in = input.digital_in;
}

void ArmInterface::RefToOutput(const RefData& ref, OutputData& output) {
  // joint
  for (size_t i = 0; i < output.motor_binary.size(); i++) {
    output.motor_binary[i] = hardware_config_.actuator.position_to_binary[i] * ref.joint.q[i] + hardware_config_.actuator.offset[i];
  }
  // digital output
  output.digital_out = ref.digital_out;
}