#include "motion_generator.h"
#include "math/math_const.h"

constexpr float SMOOTHING_TIME = 3.0f;

constexpr int NUM_REF_MODE = 37;
static const uint8_t REF_MODE_LIST[NUM_REF_MODE] = {3,1,4,5,2,0,
                                                    5,2,4,0,1,3,
                                                    2,1,0,5,4,3,
                                                    0,2,4,5,3,1,
                                                    5,1,3,2,4,0,
                                                    0,5,1,4,3,2, 6};
static const uint8_t REF_NUM_LIST[NUM_REF_MODE] =  {4,3,4,2,4,3,
                                                    2,2,2,4,2,3,
                                                    2,4,4,4,3,3,
                                                    3,2,3,3,4,4,
                                                    2,2,4,2,4,3,
                                                    3,2,2,4,3,2, 6};                                   

/**
 * @class GeneratorBase
 */
GeneratorBase::GeneratorBase() :
  state_(State::INITIALIZE) {}

GeneratorBase::~GeneratorBase() {}

void GeneratorBase::Update(float time, const JointData& state, const JointData& pre_ref, JointData& ref) {}

void GeneratorBase::StartRequest() {
  if (state_ == State::STOP) {
    state_ = State::RUN;
  }
}

void GeneratorBase::StopRequest() {
  if ((state_ == State::INITIALIZE) || (state_ == State::RUN)) {
    state_ = State::STOP;
  }
}

void GeneratorBase::CompleteConfig() {
  if (state_ == State::INITIALIZE) {
    state_ = State::STOP;
  }
}

MainGenerator::MainGenerator(uint8_t num_of_joints) :
  reference_index_(0),
  is_first_loop_(true),
  start_time_(0.0f) {}

MainGenerator::~MainGenerator() {

}

void MainGenerator::Config() {
  reference_index_ = 0;
  GetReferenceConfig(REF_MODE_LIST[0], REF_NUM_LIST[0], reference_config_);
  for (size_t i = 0; i < wave_.size(); i++) {
    wave_[i].Config(reference_config_.waves[i]);
  }
  is_first_loop_ = true;
  start_time_ = 0.0f;
}

void MainGenerator::Update(float time, const JointData& state, const JointData& pre_ref, JointData& ref) {
  // create reference
  if (state_ == State::RUN) {
    for (size_t i = 0; i < wave_.size(); i++) {
      ref.q[i] = wave_[i].Update(time);
    }
  } else {
    ref.q = state.q; // passive
  }
  // create smooth path
  if (is_first_loop_) {
    is_first_loop_ = false;
    for (size_t i = 0; i < q_traj_.size(); i++) {
      q_traj_[i].Create(pre_ref.q[i], ref.q[i], time, SMOOTHING_TIME);
    }
  }
  // modify path
  for (size_t i = 0; i < q_traj_.size(); i++) {
    if (q_traj_[i].DoSmoothing()) {
      q_traj_[i].ModifyTarget(ref.q[i]);
      ref.q[i] = q_traj_[i].Get(time);
    }
  }
    // change waves
  if ((time - start_time_) > reference_config_.operation_time) {
    // update
    reference_index_++;
    if (reference_index_ >=NUM_REF_MODE) {
      reference_index_ = 0;
    }
    GetReferenceConfig(REF_MODE_LIST[reference_index_ % NUM_REF_MODE], REF_NUM_LIST[reference_index_ % NUM_REF_MODE], reference_config_);
    for (size_t i = 0; i < wave_.size(); i++) {
      wave_[i].Config(reference_config_.waves[i]);
    }
    // reset 
    is_first_loop_ = true;
    start_time_ = time;
  }
}