#include "controller/controller.h"
#include "io_module/io_module.h"
//#include "mainte_comm/mainte_comm.h"

Controller controller_;
IoModule io_module_;
//MainteComm mainte_comm_;

void Excute() {
  //MainteData mainte;
  //mainte_comm_.Update(mainte);

  InputData input;
  io_module_.UpdateInput(input);

  OutputData output;
  controller_.Update(input, output);

  io_module_.UpdateOutput(output);
}

void setup() {
  controller_.Init();
  io_module_.Init();
  //mainte_comm_.Init();
}

void loop() {
  static unsigned long prev;
  unsigned long curr = millis();
  if ((curr - prev) >= CONTROLLER_CYCLE_TIME_MS) {
    // do periodic tasks
    prev += CONTROLLER_CYCLE_TIME_MS;
    Excute();
  }
}