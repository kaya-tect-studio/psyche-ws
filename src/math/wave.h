#ifndef WAVE_H
#define WAVE_H

enum WaveType {
    CONST,
    SIN,
    RECTANGLE,
    TRIANGLE,
    JERKMIN,
    DOWN_STEP,
    AMP_SIN,
    ECC_SIN,
    SMOOTH_STEP
};

#pragma pack(push, 1)
struct WaveForm {
  unsigned char type;
  float amplitude;
  float base;
  float frequency;
  float phase;
};
#pragma pack(pop)

class Wave {
  public:
  Wave();
  void Config(const WaveForm& form);
  void Config(unsigned char type, float amplitude, float base, float frequency, float phase);
  float Update(float time);
  const WaveForm& RefWaveForm() const {return form_; };
  private:
  WaveForm form_;
};

#endif