#include "wave.h"
#include <math.h>

#include "math_const.h"

Wave::Wave() {

}

void Wave::Config(const WaveForm& form) {
  form_ = form;
}

void Wave::Config(unsigned char type, float amplitude, float base, float frequency, float phase) {
  form_.type = type;
  form_.amplitude = amplitude;
  form_.base = base;
  form_.frequency = frequency;
  form_.phase = phase;
}

float Wave::Update(float time) {
  float ret = form_.base;
  const float ratio = fmod(time * form_.frequency + form_.phase / (2 * PI), 1.0f);
  switch(form_.type) {
    case WaveType::CONST:
    ret += 0.0f;
    break;
    case WaveType::SIN:
    ret += form_.amplitude * sin(2 * PI * ratio);
    break;
    case WaveType::RECTANGLE:
    if (ratio < 0.5f) {
      ret += form_.amplitude;
    } else {
      ret += - form_.amplitude;
    }
    break;
    case WaveType::TRIANGLE:
    if (ratio < 0.5f) {
      ret += 2 * form_.amplitude * ratio;
    } else {
      ret += 2 * form_.amplitude * (ratio - 1);
    }
    break;
    case WaveType::JERKMIN: {
      const float r = 0.25f;
      const float x = ratio + 0.5f * r - floor(ratio + 0.5f * r);
      if(x < r){
        float ts = x / r;
        ret += form_.amplitude * (- 1.0f + 2.0f * (6 * pow(ts, 5) - 15 * pow(ts, 4) + 10 * pow(ts, 3)));
      } else if (x < 0.5f) {
        ret += form_.amplitude;
      } else if (x < 0.5f + r){
        float ts = (x - 0.5f) / r;
        ret += - form_.amplitude * (- 1.0f + 2.0f * (6 * pow(ts, 5) - 15 * pow(ts, 4) + 10 * pow(ts, 3)));
      } else {
        ret += - form_.amplitude;
      }
      break;
    }
    case WaveType::DOWN_STEP: {
      unsigned char step = 3;
      ret += form_.amplitude * sin(2 * PI * ratio);
      if ((0.25f < ratio) && (ratio < 0.75f)) {
        ret += 0.5f * form_.amplitude * sin(2 * PI * (2 * step) * ratio);
      }
      break;
    }
    case WaveType::AMP_SIN: {
      float amp_ratio = 1.5f * sin(2 * PI * ratio * 0.5f);
      if (amp_ratio > 1.0f) {
        amp_ratio = 1.0f;
      } else if (amp_ratio < -1.0f) {
        amp_ratio = -1.0f;
      }
      ret += form_.amplitude * (0.25 * amp_ratio + 0.75) * sin(2 * PI * ratio);
    }
    case WaveType::ECC_SIN: {
      const float r = 0.3f;
      const float x = ratio + 0.5f * r - floor(ratio + 0.5f * r);
      if (x < r) {
        float ts = x / r;
        ret += form_.amplitude * (- 1.0f + 2.0f * (6 * pow(ts, 5) - 15 * pow(ts, 4) + 10 * pow(ts, 3)));
      } else {
        float ts = (x - r) / (1 - r);
        ret += - form_.amplitude * (- 1.0f + 2.0f * (6 * pow(ts, 5) - 15 * pow(ts, 4) + 10 * pow(ts, 3)));
      }
    }
    case WaveType::SMOOTH_STEP: {
      const float r = 0.1f;
      if (ratio < r) {
        float ts = ratio / r;
        ret += form_.amplitude * (6 * pow(ts, 5) - 15 * pow(ts, 4) + 10 * pow(ts, 3));
      } else if (ratio < (1 - r)) {
        ret += form_.amplitude;
      } else {
        float ts = (ratio - (1 - r)) / r;
        ret += form_.amplitude * (1 - (6 * pow(ts, 5) - 15 * pow(ts, 4) + 10 * pow(ts, 3)));
      }
    }
    default:
    break;
  }
  return ret;
}