#ifndef DIFFERENTIATOR_H
#define DIFFERENTIATOR_H

#include "digital_filter.h"

/**
 * @brief pseudo differentiator for velocity
 */
template<typename T>
class Differentiator {
  public:
  Differentiator(float sampling_time, const T& init_in);
  T Apply(const T& in);
  const T& Get() const;
  void SetCutoffFrequency(float frequency);
  private:
  float sampling_time_;
  T pre_in_;
  FirstLagLowPassFilter<T> filter_;
};

#include "differentiator.inl"

#endif // DIFFERENTIATOR_H