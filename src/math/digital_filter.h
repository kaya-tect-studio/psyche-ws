#ifndef DIGITAL_FILTER_H
#define DIGITAL_FILTER_H

/**
 * @brief lowpass filter using bilinear transform
 * @note out = 1 / (1 + t s) * in
 * @note t = 1 / (2 * pi * fc) time constant
 * @note s = 2 / T * (1 - z^-1) / (1 + z^-1) <- bilinear transform
 */
template<typename T>
class FirstLagLowPassFilter {
  public:
  FirstLagLowPassFilter(float sampling_time, const T& init_in);
  T Apply(const T& in);
  const T& Get() const;
  void SetCutoffFrequency(float frequency);
  private:
  float sampling_time_;;
  T pre_out_;
  T pre_in_;
  float tau_; 
};

template<typename T>
class AverageFilter {
public:
  AverageFilter(float sampling_time, float window_time, const T& init);
  T Apply(const T& in);
  const T& Get() const;
  void SetWindows(float windows_time);
private:
  float sampling_time_;
  float ave_;
};

#include "digital_filter.inl"

#endif