#ifndef MATH_CONST_H
#define MATH_CONST_H

#ifndef RAD_TO_DEG
#define RAD_TO_DEG 57.295779513082320876798154814105f
#endif

#ifndef DEG_TO_RAD
#define DEG_TO_RAD 0.017453292519943295769236907684886f
#endif

#ifndef PI
#define PI 3.14159265358979323846f
#endif

#ifndef PI_2
#define PI_2 1.57079632679489661923f
#endif

enum Axis {
  X,
  Y,
  Z,
  XYZ
};

#define S_TO_MS 1000
#define MS_TO_US 1000
#define S_TO_US (S_TO_MS * MS_TO_US)
#define MS_TO_S 0.001f
#define US_TO_MS 0.001f
#define US_TO_S (MS_TO_S * US_TO_MS)

#endif // MATH_CONST_H