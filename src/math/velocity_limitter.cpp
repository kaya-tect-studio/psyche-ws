#include "velocity_limitter.h"

VelocityLimitter::VelocityLimitter() {

}

VelocityLimitter::VelocityLimitter(float sampling_time, float fb_gain, const float max_slope) {
  sampling_time_ = sampling_time;
  fb_gain_ = fb_gain;
  max_slope_ = max_slope;
  min_slope_ = - max_slope;

  is_initialized_ = false;
}

float VelocityLimitter::Apply(float input) {
  if (!is_initialized_) { // 初期化されていない場合
    pre_input_ = input;
    pre_output_ = input;
    is_initialized_ = true;
    return input;
  }

  float vel_ref = (input - pre_input_) / sampling_time_;  //!< 目標速度
  float vel_ref_limited = (vel_ref < min_slope_) ? min_slope_ : ((vel_ref > max_slope_) ? max_slope_ : vel_ref); //!< 飽和した目標速度
  float next_output = pre_output_ + vel_ref_limited * sampling_time_;  //!< 飽和速度を積分した目標位置
  vel_ref += fb_gain_ * (input - next_output);          //!< 真の目標位置との差分をフィードバック
  vel_ref_limited = (vel_ref < min_slope_) ? min_slope_ : ((vel_ref > max_slope_) ? max_slope_ : vel_ref); //!< 飽和した目標速度
  
  pre_output_ = pre_output_ + vel_ref_limited * sampling_time_; // 出力値更新
  pre_input_ = input;                                           // 入力値更新

  return pre_output_;
}