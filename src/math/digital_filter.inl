template <typename T>
FirstLagLowPassFilter<T>::FirstLagLowPassFilter(float sampling_time, const T& init_in):
  sampling_time_(sampling_time),
  pre_out_(init_in),
  pre_in_(init_in) {}

template <typename T>
T FirstLagLowPassFilter<T>::Apply(const T& in) {
  T out = (sampling_time_) / (sampling_time_ + 2 * tau_) * (in + pre_in_)
        - (sampling_time_ - 2 * tau_) / (sampling_time_ + 2 * tau_) * pre_out_;
  // update
  pre_out_ = out;
  pre_in_ = in;
  return out;
}

template <typename T>
const T& FirstLagLowPassFilter<T>::Get() const {
  return pre_out_;
}

template <typename T>
void FirstLagLowPassFilter<T>::SetCutoffFrequency(float frequency) {
  tau_ = 1 / (2 * 3.1415f * frequency);
}

template <typename T>
AverageFilter<T>::AverageFilter(float sampling_time, float window_time, const T& init) {

}

template <typename T>
T AverageFilter<T>::Apply(const T& in) {

}

template <typename T>
const T& AverageFilter<T>::Get() const {

}

template <typename T>
void AverageFilter<T>::SetWindows(float windows_time) {
  
}