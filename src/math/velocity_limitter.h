#ifndef VELOCITY_LIMITTER_H
#define VELOCITY_LIMITTER_H

class VelocityLimitter {
public:
  VelocityLimitter();
  VelocityLimitter(float sampling_time, float fb_gain, float max_slope);

  float Apply(float input);

private:
  float sampling_time_;  //!< サンプリング時間
  float fb_gain_;        //!< 位置フィードバックゲイン
  float max_slope_;                 //!< 傾き最大値
  float min_slope_;                 //!< 傾き最小値
  float pre_input_;                      //!< 入力値
  float pre_output_;                     //!< 出力値
  bool is_initialized_;         //!< 初めてApplyを呼ばれたならtrue
};

#endif // VELOCITY_LIMITTER_H