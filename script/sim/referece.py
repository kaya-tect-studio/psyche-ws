#!/usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time

fig = plt.figure()

# 大波　小波　打ち上げ3段階　打ち下ろし3段階　押し付け

def minjerk(t):
    r = 0.25
    ratio = np.fmod(t, 2 * np.pi) / (2 * np.pi)
    x = ratio + 0.5 * r - np.floor(ratio + 0.5 * r)
    if x < r:
        ts = x / r
        ret = (- 1.0 + 2.0 * (6 * np.power(ts, 5) - 15 * np.power(ts, 4) + 10 * np.power(ts, 3)))
    elif x < 0.5:
        ret = 1
    elif x < 0.5 + r:
        ts = (x - 0.5) / r
        ret = - (- 1.0 + 2.0 * (6 * np.power(ts, 5) - 15 * np.power(ts, 4) + 10 * np.power(ts, 3)))
    else:
        ret = -1
    return ret

def ecc_sin(t):
    r = 0.2
    ratio = np.fmod(t, 2 * np.pi) / (2 * np.pi)
    x = ratio + 0.5 * r - np.floor(ratio + 0.5 * r)
    if x < r:
        ts = x / r
        ret = (- 1.0 + 2.0 * (6 * np.power(ts, 5) - 15 * np.power(ts, 4) + 10 * np.power(ts, 3)))
    else:
        ts = (x - r) / (1 - r)
        ret = - (- 1.0 + 2.0 * (6 * np.power(ts, 5) - 15 * np.power(ts, 4) + 10 * np.power(ts, 3)))
    return ret

def smooth_step(t):
    r = 0.2
    ratio = np.fmod(t, 2 * np.pi) / (2 * np.pi)
    if ratio < r:
        ts = ratio / r
        ret = (6 * np.power(ts, 5) - 15 * np.power(ts, 4) + 10 * np.power(ts, 3))
    elif ratio < (1 - r):
        ret = 1
    else:
        ts = (ratio - (1 - r)) / r
        ret = (1 - (6 * np.power(ts, 5) - 15 * np.power(ts, 4) + 10 * np.power(ts, 3)))
    return ret

def up_step(t, n):
    ur = np.sin(t)
    ratio = np.fmod(t, 2 * np.pi)
    if ratio < np.pi * 0.5 or np.pi * 1.5 < ratio:
        ur = ur + 0.3 * np.sin(n * 2 * t)
    return ur

def amp_sin(t):
    ur = np.sin(t) * (0.25 * minjerk(0.5* t) + 0.75)
    return ur

def ref_pitch_big_wave(t):
    return [35.0 * minjerk(2 * np.pi * 0.1 * t), 53.0 * np.sin(2 * np.pi * 0.1 * t - 45.0 * np.pi / 180)]

def ref_pitch_small_wave(t):
    return [17.0 * minjerk(2 * np.pi * 0.1 * t), 26.0 * np.sin(2 * np.pi * 0.1 * t - 45.0 * np.pi / 180)]

def ref_pitch_step(t):
    return [17.0 * up_step(2 * np.pi * 0.1 * t, 3), 26.0 * up_step(2 * np.pi * 0.1 * t, 3)]

def plot(data):
    t = time.time()
    pitch1, pitch2 = ref_pitch_step(t)
    origin = [0, 0]
    alpha = [280 * np.cos(pitch1 * np.pi / 180), 280 * np.sin(pitch1 * np.pi / 180)]
    beta = [280 * np.cos(pitch2 * np.pi / 180) + alpha[0], 280 * np.sin(pitch2 * np.pi / 180) + alpha[1]]
    plt.cla()
    plt.xlim(0, 560)
    plt.ylim(-560, 560)
    plt.gca().set_aspect('equal')
    plt.grid()
    plt.plot(origin[0], origin[1], '.')
    plt.plot(alpha[0], alpha[1], '.')
    plt.plot(beta[0], beta[1], '.')

def show():
    t = np.pi*np.arange(0,4,0.01)
    ref = np.zeros(len(t))
    sub = np.zeros(len(t))
    for i in range(len(ref)):
        ref[i] = np.sin(t[i])
        sub[i] = smooth_step(t[i])
    plt.grid()
    plt.plot(t, ref)
    plt.plot(t, sub)

#ani = animation.FuncAnimation(fig, plot, interval=100)
show()
plt.show()